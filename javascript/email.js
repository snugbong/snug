function email() {
    var name = document.getElementById('name').value;
    var tel = document.getElementById('tel').value;
    var money = document.getElementById('money').value;
    var area = document.getElementById('area').value;
    var style = document.getElementsByName('style');
    var stylecheck = '';
    var chk = document.getElementsByName("space[]"); // 체크박스객체를 담는다
    var len = chk.length;    //체크박스의 전체 개수
    var checkRow = '';      //체크된 체크박스의 value를 담기위한 변수
    var checkCnt = 0;        //체크된 체크박스의 개수
    var checkLast = '';      //체크된 체크박스 중 마지막 체크박스의 인덱스를 담기위한 변수
    var rowid = '';             //체크된 체크박스의 모든 value 값을 담는다
    var cnt = 0;

    if(name == "" || tel == ""){
        document.getElementById("error").innerHTML = "이름과 전화번호는 필수사항입니다";
    }else{
        document.getElementById("error").innerHTML = "";
    }

    for (var i = 0; i < len; i++) {
        if (chk[i].checked == true) {
            checkCnt++;        //체크된 체크박스의 개수
            checkLast = i;     //체크된 체크박스의 인덱스
        }
    }
    for (var i = 0; i < len; i++) {
        if (chk[i].checked == true) {  //체크가 되어있는 값 구분
            checkRow = chk[i].value;
            if (checkCnt == 1) {                            //체크된 체크박스의 개수가 한 개 일때,
                rowid += "'" + checkRow + "'";        //'value'의 형태 (뒤에 ,(콤마)가 붙지않게)
            } else {                                            //체크된 체크박스의 개수가 여러 개 일때,
                if (i == checkLast) {                     //체크된 체크박스 중 마지막 체크박스일 때,
                    rowid += "'" + checkRow + "'";  //'value'의 형태 (뒤에 ,(콤마)가 붙지않게)
                } else {
                    rowid += "'" + checkRow + "',";	 //'value',의 형태 (뒤에 ,(콤마)가 붙게)
                }
            }
            cnt++;
            checkRow = '';    //checkRow초기화.
        }
        var space = rowid;
    }

    for (var i = 0; i < style.length; i++) {
        if (style[i].checked == true) {
            stylecheck = (style[i].value);
        }
    }
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
        // xmlhttp.open("POST", "/api/send-email", true);
        xmlhttp.open("POST", "/api/send-email", true);
        // xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.setRequestHeader("Content-type", "application/json;charset=UTF-8");
        xmlhttp.onreadystatechange = function () {//Call a function when the state changes.
            // console.log("Call a function when the state changes");
            if (xmlhttp.readyState == XMLHttpRequest.DONE && xmlhttp.status == 200) {
                // Request finished. Do processing here.
                console.log("Request finished. Do processing here : " + xmlhttp.response);
            }
        };
        if(name == "" || tel == "") {

        }else{
            xmlhttp.send(JSON.stringify({
                이름: name,
                핸드폰: tel,
                예산: money,
                스타일: stylecheck,
                지역: area,
            }));
            location.href = "/";
            fbq('track', 'ContactSuccess');
        }
    }
}