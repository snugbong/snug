const Promise = require("bluebird");
const nodemailer = require('nodemailer');
let transporter;
const account = {
    user: "official@snug.kr",
    pass: "snug1234!"
};
// Generate test SMTP service account from ethereal.email
// Only needed if you don't have a real mail account for testing
nodemailer.createTestAccount((err, acc) => {

    // create reusable transporter object using the default SMTP transport
    transporter = nodemailer.createTransport({
        service: 'Gmail',
        host: 'smtp.ethereal.email',
        port: 587,
        secure: false, // true for 465, false for other ports
        ignoreTLS: false,
        auth: account
    });
});

let mailOptions = {
    from: 'official@snug.kr', // sender address
    to: 'official@snug.kr, team@sung.kr, king.star.snug@gmail.com', // list of receivers
    // from: 'vv0216vv@naver.com', // sender address
    // to: 'vv0216vv@naver.com', // list of receivers
    subject: '상담신청 메일', // Subject line
};

const service = {
    sendMail: function (user) {
        return new Promise((resolve, reject) => {
            mailOptions.text = JSON.stringify(user);
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                    return reject(error);
                }
                console.log('Message sent: %s', info.messageId);
                // Preview only available when sending through an Ethereal account
                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                resolve();
            })
        });
    }
};

module.exports = service;