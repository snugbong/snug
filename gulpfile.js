var sass = require("gulp-sass"),
    gulp = require('gulp');

// Sass
gulp.task('sass', function () {
    return gulp.src('./build/*.scss')
        .pipe(sass({outputStyle: 'compact'}).on('error', sass.logError))
        .pipe(gulp.dest('./src/sass'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./build/*.scss', ['sass']);
});